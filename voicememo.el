;;; voicememo.el --- Transcribe Voicememos and create org-headings from them. -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Vincent Trötschel
;;
;; Author: Vincent Trötschel <indiana@Indianss-MBP.fritz.box>
;; Maintainer: Vincent Trötschel <indiana@Indianss-MBP.fritz.box>
;; Created: January 11, 2024
;; Modified: January 11, 2024
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/indiana/voicememo
;; Package-Requires: ((emacs "29.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Transcribe Voicememos and create org-headings from them.
;;  TODO .lastseen is updated to refer to the audiomemo with the biggest timestamp
;;  (i.e. the latest audiomemo) of any invocation.
;;  However if one with a smaller timestamp fails it will never be retried.
;;  This is not really a problem, but maybe me need a "transcribe-memo-at-point" fn to fix that.
;;
;;; Code:
(require 'org-id)
(require 'org-element)
;;; TODO whisper.el isn't really necessary the few lines of code that are used
;;; should be placed in this file. But why not use the same config-api that way duplication
;;; can be prevented at emacs config time, i.e.:
;;; `(setq voicememo--whisper-cmd whisper-command)'
(require 'whisper)

(defconst voicememo-dirname "voice-inbox"
  "Basename of the dir that contains voicememos that are treated as gtd entries.")
(defconst voicememo-lastseen-filename ".last-seen" "Basename of the file tracking the timestamp of the last parsing operation.")
(defconst voicememo-basedir org-gtd-directory)
(defconst voicememo-inbox-filename org-gtd-inbox)
(defconst voicememo-inbox--file (concat voicememo-basedir "/" org-gtd-inbox ".org"))
(defconst voicememo-max-whisper-threads
  '(("large" . 2)
    ("base" . 10)))
(defconst voicememo-heading-length 40)
(defconst voicememo-audio-heading-hint "audio")
(defun voicememo--len-of-heading ()
  "Return length of heading after audio hint is prefixed with ': '..
Those two characters (that are visible in org mode) are substrackted,
other Characters (like '[' and ']' of the link) are not."
  (- voicememo-heading-length (length voicememo-audio-heading-hint) 2))
(defvar voicememo--curr-procs '())
(defvar voicememo--dir
  (concat voicememo-basedir "/" voicememo-dirname))
(defvar voicememo--lastseen-file
  (concat voicememo--dir "/" voicememo-lastseen-filename)
  "Full path to the `voicememo-lastseen-filename'.")
(defvar voicememo--entry-ids-alist '())

(defun voicememo--full-filepath (audiofile)
  "Return full path of basename AUDIOFILE."
  (concat voicememo--dir "/" audiofile))

;;; Don't include hidden files
;; (directory-files voicememo--dir nil "^[^.]")

(defun voicememo--str-from-file (file-path)
  "Return content of FILE-PATH as string."
  (with-temp-buffer
    (insert-file-contents file-path)
    (buffer-string)))

(defun voicememo--read-lastseen ()
  "Return last-seen timestamp."
  (let ((lastseen (voicememo--str-from-file voicememo--lastseen-file)))
    (if (string-empty-p lastseen)
        "0"
      lastseen)))

(defun voicememo--write-lastseen (str)
  "Write STR to lastseen-file."
  (write-region str nil voicememo--lastseen-file))

;; (voicememo--write-lastseen "")

(defun voicememo--strip-non-numeric (str)
  "Strip everything from STR but the numbers."
  (replace-regexp-in-string "[^0-9]" "" str))

(defun voicememo--strip-file-ext (filename)
  "Strip last 4 chars from FILENAME."
  (substring-no-properties filename 0 -4))

(defun voicememo--file-timestamp (audiofile)
  "Get timestamp from filename AUDIOFILE."
  (string-to-number
   (voicememo--strip-non-numeric
    (voicememo--strip-file-ext audiofile))))

(defun voicememo--read-lastseen-timestamp ()
  "Convert content of .lastseen to timestamp."
  (string-to-number
   (voicememo--strip-non-numeric
    (voicememo--read-lastseen))))

(defun voicememo--recent-vms ()
  "Get all voicememos more recent that the timestamp in .lastseen."
  (seq-filter (lambda (audiofile)
                (> (voicememo--file-timestamp audiofile)
                   (voicememo--read-lastseen-timestamp)))
              (directory-files voicememo--dir nil "^[^.]")))

(defun voicememo--rel-file-link (audiofile)
  "Return string that corresponds to an org-link to AUDIOFILE."
  (format "[[file:%s/%s][%s]]"
          voicememo-dirname
          audiofile
          voicememo-audio-heading-hint))

(defun voicememo--inbox-heading (audiofile)
  "String that is inserted to create an org-heading for AUDIOFILE."
  (format "* %s %s"
          (voicememo--strip-file-ext audiofile)
          (voicememo--rel-file-link audiofile)))

(defun voicememo--whisper-buffername-stdout (audiofile)
  "Name of stdout buffer for AUDIOFILEs transcription process."
  (concat "*voicememo-whisper-"
          (voicememo--strip-file-ext audiofile)
          "-stdout*"))

(defun voicememo--whisper-buffername-stderr (audiofile)
  "Name of stderr buffer for AUDIOFILEs transcription process."
  (concat "*voicememo-whisper-"
          (voicememo--strip-file-ext audiofile)
          "-stderr*"))

(defun voicememo--queue-whisper-proc (whisper-fn)
  "Run WHISPER-FN in new proc.
Check if the number of existing processes is less than max-procs first."
  (let ((max-procs (alist-get whisper-model voicememo-max-whisper-threads
                              1 nil #'string-equal)))
    (message "try to start proc %d/%d" (+ 1 (length voicememo--curr-procs)) max-procs)
    (if (< (length voicememo--curr-procs)
           max-procs)
        (funcall whisper-fn)
      (run-with-timer 3 nil
                      (lambda () (voicememo--queue-whisper-proc whisper-fn))))))

(defun voicememo--process-done-event-p (event)
  "Return t if EVENT is the last signal of a process."
  (or (string-equal "finished\n" event)
      (string-equal "terminated\n" event)
      (string-equal "exited abnormally with code 255\n" event)))

(defun voicememo--filename->orgid (audiofile)
  "Return org-id of AUDIOFILE heading."
  (alist-get audiofile voicememo--entry-ids-alist
             nil nil #'string-equal))

(defun voicememo--update-heading-title* (heading-id new-title)
  "Find Org mode heading by HEADING-ID and update its title to NEW-TITLE."
  (let ((el-marker (org-id-find heading-id 'marker)))
    (when el-marker
      (save-excursion
        (with-current-buffer (marker-buffer el-marker)
          (goto-char (marker-position el-marker))
          (let ((current-level (org-element-property :level (org-element-at-point))))
            (if current-level
                (progn
                  (delete-region (point) (eol))
                  (insert (format "%s %s" (make-string current-level ?*) new-title))
                  ;; (org-id-update-id-locations (point) (point))
                  (message "Title of heading %s updated." heading-id))
              (message "Error: Unable to determine current level of heading %s." heading-id))))))
    (unless el-marker
      (message "Heading with ID %s not found." heading-id))))

(defun voicememo--update-heading-title (audiofile new-title)
  "Find Org mode heading of AUDIOFILE and update its title to NEW-TITLE."
  (voicememo--update-heading-title* (voicememo--filename->orgid audiofile)
                                    new-title))

(defun voicememo--set-heading-content* (heading-id content)
  "Find Org mode heading by HEADING-ID and set its content to CONTENT."
  (if (or (not heading-id) (string-empty-p heading-id))
      (message "No org-id provided")
    (let ((el-marker (org-id-find heading-id 'marker)))
      (if (not el-marker)
          (message "Heading with ID %s not found." heading-id)
        (save-excursion
          (with-current-buffer (marker-buffer el-marker)
            (goto-char (marker-position el-marker))
            (org-end-of-meta-data)
            ;; (delete-region (point) (org-end-of-subtree t t))
            (insert content)
            ;; (org-id-update-id-locations (point) (point))
            ))))))

(defun voicememo--set-heading-content (audiofile content)
  "Find Org mode heading of AUDIOFILE and set its content to CONTENT."
  (voicememo--set-heading-content* (voicememo--filename->orgid audiofile)
                                   content))

(defun voicememo--insert-transcription (audiofile transcript)
  "Insert TRANSCRIPT into heading of AUDIOFILE.
Wraps it in a quote-block and appends extra '\n'.
It also prepends the result of `voicememo--org-timestamp',
this way entries resemble `org-gtd'."
  (voicememo--set-heading-content
   audiofile (concat (voicememo--org-timestamp audiofile)
                     "\n#+begin_quote\n"
                     transcript
                     "\n#+end_quote\n\n")))

(defun voicememo--truncate-string-with-ellipsis (str max-length)
  "Truncate STR to MAX-LENGTH characters and append ellipsis if needed."
  (if (<= (length str) max-length)
      str
    (concat (substring str 0 (- max-length 2)) "…")))

(defun voicememo--lisp-time (audiofile)
  "Extract timestamp from AUDIOFILE and return it as lisp time-obj."
  (pcase-let
      ((`(,YEAR ,MON ,DAY ,HOUR ,MIN ,SEC) (string-split
                                            (voicememo--strip-file-ext audiofile)
                                            "_"))
       (DST -1)                         ;`encode-time' can guess daylight saving t
       (TZ nil))                        ;`encode-time' emacs-local timezone
    (encode-time
     (mapcar (lambda (el)
               (if (stringp el)
                   (string-to-number el)
                 el))
             (list SEC MIN HOUR DAY MON YEAR nil DST TZ)))))

(defun voicememo--org-timestamp (audiofile)
  "Returns inactive timestamp with time extracted from AUDIOFILE."
  (format-time-string
   (concat "[" (cdr org-timestamp-formats) "]")
   (voicememo--lisp-time audiofile)))

(defun voicememo--set-property* (heading-id key value)
  "For heading with HEADING-ID insert into property drawer: KEY VALUE."
  (if (or (not heading-id) (string-empty-p heading-id))
      (message "No org-id provided")
    (let ((el-marker (org-id-find heading-id 'marker)))
      (if (not el-marker)
          (message "Heading with ID %s not found." heading-id)
        (save-excursion
          (with-current-buffer (marker-buffer el-marker)
            (org-set-property key value)))))))

(defun voicememo--set-property (audiofile key value)
  "For heading of AUDIOFILE insert into property drawer: KEY VALUE."
  (voicememo--set-property* (voicememo--filename->orgid audiofile)
                            key value))

(defun voicememo--whisper-transcribe (audiofile)
  "Start audio transcribing process of AUDIOFILE in the background."
  (let ((fpath (voicememo--full-filepath audiofile))
        (stdout-buffername (voicememo--whisper-buffername-stdout audiofile))
        (stderr-buffername (voicememo--whisper-buffername-stderr audiofile)))
    (message (concat "[-] Queue audio for transcription: " audiofile))
    (voicememo--queue-whisper-proc
     (lambda ()
       (message "[START] transcribing %s" audiofile)
       (setq voicememo--curr-procs
             (cons (make-process
                    :name (concat "whisper-transcribing-" audiofile)
                    ;; TODO dependency on whisper pkg
                    :command (whisper-command fpath)
                    :connection-type nil
                    :buffer (get-buffer-create stdout-buffername)
                    :stderr (get-buffer-create stderr-buffername)
                    :coding 'utf-8
                    :sentinel
                    (lambda (proc event)
                      (unwind-protect
                          (when-let* ((stdout-buffer (get-buffer stdout-buffername))
                                      (finished (and (buffer-live-p stdout-buffer)
                                                     (string-equal "finished\n" event))))
                            (with-current-buffer stdout-buffer
                              (goto-char (point-min))
                              (skip-chars-forward " \n")
                              (when (> (point) (point-min))
                                (delete-region (point-min) (point)))
                              (goto-char (point-max))
                              (skip-chars-backward " \n")
                              (when (> (point-max) (point))
                                (delete-region (point) (point-max)))
                              (when (= (buffer-size) 0)
                                (error "Whisper command produced no output"))
                              (goto-char (point-min))
                              ;; TODO dependency on whisper pkg
                              (run-hooks 'whisper-post-process-hook)
                              (when (> (buffer-size) 0)
                                (let ((transcription (buffer-string)))
                                  (voicememo--insert-transcription
                                   audiofile transcription)
                                  (voicememo--update-heading-title
                                   audiofile
                                   (concat
                                    (voicememo--rel-file-link audiofile)
                                    ": /" (voicememo--truncate-string-with-ellipsis
                                           transcription (voicememo--len-of-heading)) "/"))))
                              (if (> (voicememo--file-timestamp audiofile)
                                     (voicememo--read-lastseen-timestamp))
                                  (voicememo--write-lastseen
                                   (voicememo--strip-file-ext audiofile)))))
                        (when (voicememo--process-done-event-p event)
                          (message "[STOP] transcribing %s (received %s)"
                                   audiofile event)
                          ;; Ensure process is dead to prevent `kill-buffer' prompt
                          (ignore-errors (kill-process proc))
                          (setq voicememo--curr-procs
                                (delq proc voicememo--curr-procs))
                          (ignore-errors (kill-buffer stdout-buffername))))))
                   voicememo--curr-procs))))))

(defun voicememo--create-entry (audiofile)
  "Create an org entry in the inbox for AUDIOFILE."
  (let ((file-path (voicememo--full-filepath audiofile)))
    (if (not (file-exists-p file-path))
        (message "can't find file: %s" file-path)
      (with-current-buffer (get-file-buffer voicememo-inbox--file)
        (goto-char (point-max))
        (insert
         "\n" (voicememo--inbox-heading audiofile) "\n")
        (let ((id (org-id-get-create)))
          (setq voicememo--entry-ids-alist
                (cons (cons audiofile id)
                      voicememo--entry-ids-alist)))))))

(defun voicememo-scan ()
  "Scan `voicememo--dir' for recent additions.
For each one add an entry to `voicememo-inbox--file'.
Voicememos are parsed locally with the whisper model from openai."
  (interactive)
  (if-let ((memos (voicememo--recent-vms)))
      (mapc (lambda (audiofile)
              (voicememo--create-entry audiofile)
              (voicememo--set-property audiofile "RECORDED"
                                       (voicememo--org-timestamp
                                        audiofile))
              (voicememo--whisper-transcribe audiofile))
            memos)
    (message "No new Voicememos found.")))

(provide 'voicememo)
;;; voicememo.el ends here
